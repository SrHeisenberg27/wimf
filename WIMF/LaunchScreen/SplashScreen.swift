//
//  SplashScreen.swift
//  WIMF
//
//  Created by Álvaro Ferrández Gómez on 09/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class SplashScreen: UIViewController {

    var timer: Timer?
    var timeCount: Int = 0
    let animationSeconds: Int = 2

    override func viewDidLoad() {
        super.viewDidLoad()

        setTimerAndAnimateLaunchScreen()
    }

    func setTimerAndAnimateLaunchScreen() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkForTimerAndRedirect), userInfo: nil, repeats: true)
    }

    @objc func checkForTimerAndRedirect() {
        if timeCount == animationSeconds {
            timer?.invalidate()
            timer = nil

            guard let homeVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
                return
            }

            let navigationController = UINavigationController(rootViewController: homeVC)

            UIApplication.shared.windows.first?.rootViewController = navigationController
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            timeCount += 1
        }
    }
}
