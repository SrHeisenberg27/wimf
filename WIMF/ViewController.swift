//
//  ViewController.swift
//  WIMF
//
//  Created by Álvaro Ferrández Gómez on 09/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet public weak var buttonPlay: UIImageView!
    @IBOutlet public weak var buttonStop: UIImageView!
    @IBOutlet public weak var buttonReplay: UIImageView!

    var player: AVAudioPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setGestures()
        self.setPlayer()
    }

    func setGestures() {
        let play = UITapGestureRecognizer(target: self, action: #selector(play(tapGestureRecognizer:)))
        buttonPlay.isUserInteractionEnabled = true
        buttonPlay.addGestureRecognizer(play)

        let pause = UITapGestureRecognizer(target: self, action: #selector(stop(tapGestureRecognizer:)))
        buttonStop.isUserInteractionEnabled = true
        buttonStop.addGestureRecognizer(pause)

        let replay = UITapGestureRecognizer(target: self, action: #selector(replay(tapGestureRecognizer:)))
        buttonReplay.isUserInteractionEnabled = true
        buttonReplay.addGestureRecognizer(replay)
    }

    @objc func play(tapGestureRecognizer: UITapGestureRecognizer) {
        self.player?.play()
    }

    @objc func stop(tapGestureRecognizer: UITapGestureRecognizer) {
        self.player?.pause()
    }

    @objc func replay(tapGestureRecognizer: UITapGestureRecognizer) {
        self.player?.stop()
        self.player?.currentTime = 0
        self.player?.play()
    }

    func setPlayer() {
        guard let url = Bundle.main.url(forResource: "himno_eeuu", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            player?.prepareToPlay()
            player?.play()
            
            let alert = UIAlertController(title: "WIMF", message: "Sube el volumen por la libertad", preferredStyle: .alert)
            self.present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline:  DispatchTime.now() + 5){
              alert.dismiss(animated: true, completion: nil)
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
}

